
import 'package:remoteflutter/net/request_model_mapper.dart';
import 'package:remoteflutter/net/response_model_mapper.dart';

abstract class HttpRemoteRequest {
  Future<ResponseModelMapper> post(
      RequestModelMapper request, ResponseModelMapper responseMapper);

  Future<ResponseModelMapper> get(ResponseModelMapper responseMapper);

  Future<List<ResponseModelMapper>> getList(ResponseModelMapper responseMapper);
}
