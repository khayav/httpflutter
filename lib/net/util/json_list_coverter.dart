import 'dart:async' show Future;

import 'package:flutter/services.dart' show rootBundle;
import 'package:remoteflutter/net/response_model_mapper.dart';

///@Class T extends ResponseModelMapper for deserialization
///ListConverter converts json array into Model Mapper Type List
class JsonConverter<T extends ResponseModelMapper> {
  ///@param classType Defines a instance of return item
  ///@param result Raw api response dynamic data type
  ///@convert returns List of  dataType T extends  ResponseModelMapper
  List<T> convertToList(T classType, dynamic jsonResultContent) {
    var items = List<T>(); // define list of class type instance
    var jsonList = jsonResultContent as List; // Cast Response to List
    var index = 0;
    while (index < jsonList.length) {
      Map map = jsonList[index] as Map;
      items.add(classType.fromMap(map));
      index++;
    }
    return items;
  }

  T convert(T classType, dynamic jsonResultContent) {
    return classType != null ? classType.fromMap(jsonResultContent) : null;
  }

  static Future<dynamic> loadLocalData(String fileName) async {
    return await rootBundle.loadString('assets/' + fileName);
  }
}

class SecretLoader {
  final String secretPath = 'assets/structures.json';

  Future<dynamic> load() {
    return rootBundle.loadStructuredData<dynamic>(this.secretPath,
        (jsonStr) async {
      print(jsonStr);
      return dynamic;
    });
  }
}
