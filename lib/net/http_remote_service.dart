import 'dart:convert' show json;

import 'package:http/http.dart' as http;
import 'package:remoteflutter/exceptions/exceptions.dart';
import 'package:remoteflutter/net/HttpClientFactory.dart';
import 'package:remoteflutter/net/http_remote_request.dart';
import 'package:remoteflutter/net/request_model_mapper.dart';
import 'package:remoteflutter/net/response_model_mapper.dart';
import 'package:remoteflutter/net/util/json_list_coverter.dart';

class HttpRemoteService implements HttpRemoteRequest {
  final TAG = "[RemoteService]";

  HttpClientFactory _httpClient;

  HttpRemoteService(this._httpClient);

  @override
  Future<ResponseModelMapper> post(
      RequestModelMapper request, ResponseModelMapper responseMapper) async {
    if (responseMapper == null) {
      throw new FetchDataException(
          "Initialize response ocurred : [Response null]");
    }
    print(TAG + "Sending post" + _httpClient.buildUrl());
    print(TAG + "Body:" + json.encode(request.toMap()));
    var results = await http
        .post(_httpClient.buildUrl(), body: request.toMap())
        .timeout(_httpClient.duration);
    print("${results.statusCode}");
    print(TAG + "Results " + results.statusCode.toString());
    print("${results.body}");
    print(TAG + "Body Results " + results.body);
    final statusCode = results.statusCode;

    if (statusCode != 200) {
      throw new FetchDataException(
          "An error ocurred : [Status Code : $statusCode]");
    } else {
      var response = responseMapper.fromMap(json.decode(results.body));
      return response;
    }
  }

  @override
  Future<ResponseModelMapper> get(ResponseModelMapper responseMapper) async {
    if (responseMapper == null) {
      throw new FetchDataException(
          "Initialize response ocurred : [Mapper is null]");
    }
    print(TAG + "Sending get" + _httpClient.buildUrl());
    var response =
        await http.get(_httpClient.buildUrl()).timeout(_httpClient.duration);
    print(TAG + "response from get: " + _httpClient.buildUrl());
    print("${response.body}");
    final statusCode = response.statusCode;
    if (statusCode != 200) {
      throw new FetchDataException(
          "An error ocurred : [Status Code : $statusCode]");
    } else {
      return responseMapper.fromMap(json.decode(response.body));
    }
  }

  @override
  Future<List<ResponseModelMapper>> getList(
      ResponseModelMapper responseMapper) async {
    if (responseMapper == null) {
      throw new FetchDataException(
          "Initialize response ocurred : [Mapper is null]");
    }

    var response = await http.get(_httpClient.buildUrl()).timeout(_httpClient.duration);
    print("${response.body}");
    final statusCode = response.statusCode;
    if (statusCode != 200) {
      throw new FetchDataException(
          "An error ocurred : [Status Code : $statusCode]");
    } else {
      return new JsonConverter()
          .convertToList(responseMapper, json.decode(response.body));
    }
  }
}