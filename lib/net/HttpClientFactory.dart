import 'package:remoteflutter/exceptions/exceptions.dart';
import 'package:remoteflutter/net/url_builder.dart';

///Creates of http client @[HttpRemoteService]
class HttpClientFactory implements UrlBuilder {
  final TAG = "[HttpClientFactory]";
  Duration duration;
  Map headers;
  String _host;
  Map _params;
  Map _pathVariables;
  String _endPoint;

  @override
  UrlBuilder addHost(String host) {
    this._host = host;
    return this;
  }

  @override
  UrlBuilder addTimeout(var timeout) {
    this.duration = Duration(milliseconds: timeout);
    return this;
  }

  @override
  UrlBuilder addHeaders(Map headers) {
    this.headers = headers;
    return this;
  }

  @override
  UrlBuilder addQueryParams(Map<String, dynamic> params) {
    this._params = params;
    return this;
  }

  @override
  UrlBuilder addPathVariable(Map<String, dynamic> params) {
    this._pathVariables = params;
    return this;
  }

  @override
  UrlBuilder endEndpoint(String endPoint) {
    this._endPoint = endPoint;
    return this;
  }

  @override
  String buildUrl() {
    if (_endPoint == null) {
      throw new FetchDataException("endPoint can not be null");
    }
    if (_pathVariables != null) {
      for (MapEntry m in _pathVariables.entries) {
        _endPoint = _endPoint.replaceAll("{" + m.key + "}", m.value.toString());
      }
    }
    String pathParams = "";
    if (_params != null) {
      for (MapEntry m in _params.entries) {
        pathParams = pathParams.length > 0
            ? "?" + pathParams + "&" + m.key + "=" + m.value.toString()
            : pathParams = pathParams + m.key + "=" + m.value.toString();
      }
    }
    return _host + _endPoint + pathParams;
  }
}
