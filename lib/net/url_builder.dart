abstract class UrlBuilder {
  UrlBuilder addHost(String host);

  UrlBuilder addTimeout(int timeout);

  UrlBuilder addHeaders(Map headers);

  UrlBuilder addQueryParams(Map<String, dynamic> params);

  UrlBuilder addPathVariable(Map<String, dynamic> params);

  UrlBuilder endEndpoint(String endPoint);

  String buildUrl();
}
