

import 'package:remoteflutter/net/response_model_mapper.dart';

class SampleResponse implements ResponseModelMapper {
  String registrationId;
  String errorMessage;

  @override
  ResponseModelMapper fromMap(dynamic resultsMap) {
    registrationId = resultsMap['RegistrationId'];
    errorMessage = resultsMap['ErrorMessage'];
    return this;
  }
}
