

import 'package:remoteflutter/net/response_model_mapper.dart';

class SampleResponseItem implements ResponseModelMapper {
  var itemId;
  var storeId;
  var name;
  var description;
  var price;
  var wieghed;
  var imageUrl;
  var creation;
  var lastUpdate;

  @override
  ResponseModelMapper fromMap(dynamic resultsMap) {
    this.itemId = resultsMap["itemId"];
    this.storeId = resultsMap["storeId"];
    this.name = resultsMap["name"];
    this.description = resultsMap["description"];
    this.wieghed = resultsMap["wieghed"];
    this.imageUrl = resultsMap["imageUrl"];
    this.creation = resultsMap["creation"];
    this.lastUpdate = resultsMap["lastUpdate"];
    return this;
  }
}
