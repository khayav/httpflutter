

import 'package:remoteflutter/net/request_model_mapper.dart';

class SampleRequest implements RequestModelMapper {
  String userLogon;
  String userPwd;
  String imei;
  String device;
  String phoneNumber;

  @override
  Map toMap() {
    return {
      'UserLogon': userLogon,
      'UserPwd': userPwd,
      'IMEI': imei,
      "Device": device,
      'PhoneNumber': phoneNumber
    };
  }
}
