import 'package:remoteflutter/net/response_model_mapper.dart';
import 'package:remoteflutter/net/util/json_list_coverter.dart';
import 'package:remoteflutter/sample/SampleResponseItem.dart';

class SampleItemList implements ResponseModelMapper {
  List<SampleResponseItem> items;

  @override
  ResponseModelMapper fromMap(dynamic result) {
    items = JsonConverter<SampleResponseItem>().convertToList(SampleResponseItem(), result);
    return this;
  }
}
