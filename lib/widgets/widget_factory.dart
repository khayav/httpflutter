import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

createInputFieldWithIcon(
    IconData icon, var controller, var label, bool obscure) {
  return new Container(
      padding: const EdgeInsets.only(top: 10.0),
      child: new TextFormField(
          controller: controller,
          obscureText: obscure,
          keyboardType: TextInputType.text,
          // Use email input type for emails.
          decoration: new InputDecoration(
            labelText: label,
            icon: new Icon(icon),
          )));
}
