import 'package:flutter_test/flutter_test.dart';
import 'package:remoteflutter/net/HttpClientFactory.dart';

import 'package:remoteflutter/net/http_remote_service.dart';
import 'package:remoteflutter/sample/SampleItemList.dart';
import 'package:remoteflutter/sample/SampleResponseItem.dart';

void main() {
  test('Test SampleResponsetItem GET', () async {
    final remote = HttpRemoteService(HttpClientFactory()
        .addHost("http://localhost:8081/api/")
        .addTimeout(60000)
        .endEndpoint("item/getbyid/{id}")
        .addPathVariable({"id": 12}).addHeaders(
            {'Content-Type': "application/json"}));
    var sampleResponse = SampleResponseItem();

    await remote
        .get(SampleResponseItem())
        .then((response) => {sampleResponse = response});
    expect(sampleResponse.itemId, 12);
  });

  test('Test SampleResponsetItem GET List', () async {
    bool isSuccess = false;

    final remote = HttpRemoteService(HttpClientFactory()
        .addHost("http://localhost:8081/api/")
        .addTimeout(60000)
        .endEndpoint("item/all"));
    var sampleItemListResponse = SampleItemList();

    await remote
        .get(SampleItemList())
        .then((response) => {sampleItemListResponse = response});
    isSuccess = sampleItemListResponse.items.length > 0;
    expect(isSuccess, true);
  });
}
